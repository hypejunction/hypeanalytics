<?php

/**
 * Adds an event to tracking batch
 *
 * @param string $event			Name of the event, e.g. 'Completed Order'
 * @param array $properties		An array of properties describing this event
 * @return void
 */
function analytics_track_custom_event($event, $properties) {

	elgg_trigger_plugin_hook('track', 'system', array(
		'event' => $event,
		'properties' => $properties
	));
}