<?php

$url = elgg_view('output/url', array(
	'text' => 'donation',
	'href' => "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=P7QA9CFMENBKA",
		));

elgg_add_admin_notice('gpl.donate', "Developing and maintaining open source plugins requires considerable effort and is often a thankless undertaking. Consider a $url if you benefit from using this and other hypeJunction plugins");
