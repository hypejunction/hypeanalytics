<?php

namespace hypeJunction\Analytics;

use ElggCrypto;

class Segmentio {

	static $traits;
	static $batch;

	protected static function getSessionHash() {
		if (!isset($_SESSION['analytics_session_hash'])) {
			$_SESSION['analytics_session_hash'] = ElggCrypto::getRandomString(32, ElggCrypto::CHARS_HEX);
		}

		return $_SESSION['analytics_session_hash'];
	}

	public static function getUserId() {
		if (elgg_is_logged_in()) {
			return elgg_get_logged_in_user_entity()->username;
		}
		return self::getSessionHash();
	}

	public static function addToBatch($params) {

		if (!isset(self::$batch)) {
			self::$batch = array();
		}

		if (is_array($params)) {
			self::$batch[] = $params;
		}

		return self::$batch;
	}

	public static function getTraits($userId) {

		if (!isset(self::$traits)) {
			self::$traits = array();
		}

		if (!isset(self::$traits[$userId])) {
			$traits = array();
			$user = get_entity($userId);
			$traits = self::getUserProperties($user);
			self::$traits[$userId] = $traits;
		}

		return self::$traits[$userId];
	}

	public static function identify() {

		$userId = self::getUserId();
		$traits = self::getTraits($userId);

		self::addToBatch(array(
			'action' => 'identify',
			'userId' => $userId,
			'traits' => $traits,
			'timestamp' => date(DATE_ISO8601)
		));

		return true;
	}

	public static function alias($event, $type, $user) {

		self::addToBatch(array(
			'action' => 'alias',
			'from' => self::getSessionHash(),
			'to' => $user->username,
			'timestamp' => date(DATE_ISO8601)
		));

		return true;
	}

	public static function trackAction($hook, $action, $return, $params) {

		if (!elgg_get_plugin_setting('track_action', PLUGIN_ID)) {
			return $return;
		}
		
		$userId = self::getUserId();

		$properties = array(
			'guid' => get_input('guid'),
			'container_guid' => get_input('container_guid'),
			'owner_guid' => get_input('owner_guid'),
		);

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => elgg_echo('analytics_action', array($action)),
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return $return;
	}

	public static function trackForward($hook, $reason, $location, $params) {

		if (!elgg_get_plugin_setting('track_forward', PLUGIN_ID)) {
			return $location;
		}

		$userId = self::getUserId();

		$properties = $params;

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => elgg_echo('analytics_forward', array($reason)),
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return $location;
	}

	public static function trackRouter($hook, $handler, $return, $params) {

		if (!elgg_get_plugin_setting('track_route', PLUGIN_ID)) {
			return $return;
		}

		$userId = self::getUserId();

		$properties = array(
			'segments' => $params['segments']
		);

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => elgg_echo('analytics_router', array($params['handler'])),
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return $return;
	}

	public static function trackView($hook, $view, $return, $params) {

		$tracked_views = array(
			'profile/details',
			'groups/profile/layout',
		);

		$entities = elgg_get_config('registered_entities');
		foreach ($entities as $type => $subtypes) {
			if (is_array($subtypes)) {
				foreach ($subtypes as $subtype) {
					$tracked_views[] = "$type/$subtype";
				}
			} else {
				$tracked_views[] = "$type/default";
			}
		}

		if (!in_array($view, $tracked_views)) {
			return $return;
		}
		
		$userId = self::getUserId();
		$entity = $params['vars']['entity'];

		if (elgg_instanceof($entity)) {
			$type_descriptor = "analytics_item";
			if ($type = $entity->getType()) {
				$type_descriptor .= ":$type";
			}
			if ($subtype = $entity->getSubtype()) {
				$type_descriptor .= ":$subtype";
			}
		}
		if ($params['vars']['full_view']) {
			$event = elgg_echo("analytics_view:full", array(elgg_echo($type_descriptor)));
		} else {
			$event = elgg_echo("analytics_view:summary", array(elgg_echo($type_descriptor)));
		}

		switch ($type) {
			case 'user' :
				$properties = self::getUserProperties($entity);
				break;

			case 'object' :
				$properties = self::getObjectProperties($entity);
				break;

			case 'group' :
				$properties = self::getGroupProperties($entity);
				break;
		}

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => $event,
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return $return;
	}

	public static function trackEvent($event, $type, $object) {

		$userId = self::getUserId();

		if (is_object($object)) {
			$entity = $object;
		} else if (is_array($object)) {
			$entity = elgg_extract('entity', $object, null);
			if (!$entity) {
				$entity = elgg_extract('user', $object, null);
			}
			if (!$entity) {
				$entity = elgg_extract('group', $object, null);
			}
		}

		if ($entity) {
			$type_descriptor = "analytics_item";
			if (method_exists($entity, 'getType')) {
				$entity_type = $entity->getType();
				if ($entity_type) {
					$type_descriptor .= ":$entity_type";
				}
			}
			if (method_exists($entity, 'getSubtype')) {
				$entity_subtype = $entity->getSubtype();
				if ($entity_subtype) {
					$type_descriptor .= ":$entity_subtype";
				}
			}

			$event = elgg_echo("analytics_event::$event::$type");
			$type_descriptor = elgg_echo($type_descriptor);

			switch ($entity_type) {

				default :
					$properties = (array) $entity;
					break;

				case 'user' :
					$properties = self::getUserProperties($entity);
					break;

				case 'object' :
					$entities = elgg_get_config('registered_entities');
					if (!in_array($entity_subtype, $entities['object'])) {
						return true;
					}
					$properties = self::getObjectProperties($entity);
					break;

				case 'group' :
					$properties = self::getGroupProperties($entity);
					break;

				case 'relationship' :
					$properties = array(
						'type' => 'relationship',
						'id' => $entity->id,
						'name' => $entity->name,
						'guid' => array($entity->guid_one, $entity->guid_two)
					);
					break;

				case 'metadata' :
				case 'annotation' :
					$properties = array(
						'type' => $entity_type,
						'id' => $entity->id,
						$entity->name => $entity->value,
					);
					break;
			}
		}

		if (empty($type_descriptor)) {
			$type_descriptor = $type;
		}

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => "$event $type_descriptor",
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return true;
	}

	public static function trackCustom($hook, $action, $return, $params) {

		$event = elgg_extract('event', $return);
		$properties = elgg_extract('properties', $return);

		$userId = self::getUserId();

		self::addToBatch(array(
			'action' => 'track',
			'userId' => $userId,
			'event' => $event,
			'properties' => array_filter($properties),
			'context' => array(
				'library' => 'hypeAnalytics',
			),
			'timestamp' => date(DATE_ISO8601)
		));

		return $return;
	}

	public static function postData() {

		elgg_trigger_plugin_hook('analytics:data', 'system', array(
			'data' => self::$batch,
		));

		$write_key = HYPEANALYTICS_SEGMENTIO_WRITE_KEY;
		if (!$write_key) {
			return true;
		}

		if (!isset(self::$batch) || count(self::$batch) <= 1) {
			return true;
		}

		$post = json_encode(array(
			'secret' => $write_key,
			'batch' => self::$batch
		));

		$ch = curl_init('https://api.segment.io/v1/import');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($post))
		);
		
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$json = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($json);
		if ($response->error->code != 200) {
			elgg_log("Posting batch of events to Segmentio.io failed: " . $response->error->message, 'WARNING');
		} else {
			unset(self::$batch);
		}
	}

	public static function getUserProperties($entity) {
		if (!elgg_instanceof($entity, 'user')) {
			return false;
		}

		$properties = array(
			'type' => 'user',
			'subtype' => $entity->getSubtype(),
			'url' => $entity->getURL(),
			'guid' => $entity->guid,
			'name' => $entity->name,
			'username' => $entity->username,
			'email' => $entity->email,
			'admin' => $entity->isAdmin(),
			'language' => $entity->language,
			'description' => $entity->briefdescription,
			'interests' => $entity->interests,
			'skills' => $entity->skills,
			'location' => $entity->getLocation(),
			'latitude' => $entity->getLatitude(),
			'longitude' => $entity->getLongitude(),
		);

		return elgg_trigger_plugin_hook('analytics:properties', 'user', array(
			'entity' => $entity,
				), $properties);
	}

	public static function getObjectProperties($entity) {
		if (!elgg_instanceof($entity, 'object')) {
			return false;
		}

		$properties = array(
			'type' => 'object',
			'subtype' => $entity->getSubtype(),
			'url' => $entity->getURL(),
			'guid' => $entity->guid,
			'title' => $entity->title,
			'tags' => $entity->tags,
			'location' => $entity->getLocation(),
			'latitude' => $entity->getLatitude(),
			'longitude' => $entity->getLongitude(),
		);

		return elgg_trigger_plugin_hook('analytics:properties', 'object', array(
			'entity' => $entity,
				), $properties);
	}

	public static function getGroupProperties($entity) {
		if (!elgg_instanceof($entity, 'group')) {
			return false;
		}

		$properties = array(
			'type' => 'group',
			'subtype' => $entity->getSubtype(),
			'url' => $entity->getURL(),
			'guid' => $entity->guid,
			'name' => $entity->name,
			'tags' => $entity->tags,
			'location' => $entity->getLocation(),
			'latitude' => $entity->getLatitude(),
			'longitude' => $entity->getLongitude(),
		);

		return elgg_trigger_plugin_hook('analytics:properties', 'object', array(
			'entity' => $entity,
				), $properties);
	}

}
