<?php

use hypeJunction\Analytics\Segmentio;

$segmentio_write_key = HYPEANALYTICS_SEGMENTIO_WRITE_KEY;
$userId = Segmentio::getUserId();

$analytics = json_encode(array(
	'segmentio_write_key' => $segmentio_write_key,
	'userId' => $userId,
	'userTraits' => Segmentio::getTraits($userId),
	'pageContext' => elgg_get_context(),
		));

echo <<<__JS
elgg.config.analytics = $analytics;
__JS;

