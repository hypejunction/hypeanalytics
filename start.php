<?php

/**
 * Analytics for Elgg
 *
 * @package hypeJunction
 * @subpackage Analytics
 *
 * @author Ismayil Khayredinov <ismayil@hypejunction.com>
 * @copyright Copyright (c) 2011-2014, Ismayil Khayredinov
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

namespace hypeJunction\Analytics;

const PLUGIN_ID = 'hypeAnalytics';
const PAGEHANDLER = 'analytics';

include __DIR__ . '/lib/global.php';

elgg_register_class('hypeJunction\\Analytics\\Segmentio', __DIR__ . '/classes/hypeJunction/Analytics/Segmentio.php');

define('HYPEANALYTICS_SEGMENTIO_READ_KEY', elgg_get_plugin_setting('segmentio_read_key', PLUGIN_ID));
define('HYPEANALYTICS_SEGMENTIO_WRITE_KEY', elgg_get_plugin_setting('segmentio_write_key', PLUGIN_ID));

elgg_register_event_handler('init', 'system', __NAMESPACE__ . '\\init');

function init() {

	if (!HYPEANALYTICS_SEGMENTIO_WRITE_KEY) {
		if (!elgg_admin_notice_exists('analytics:segmentio_keys')) {
			elgg_add_admin_notice('analytics:segmentio_keys', elgg_echo('analytics:admin_notice:segmentio_keys'));
		}
	}

	elgg_extend_view('js/initialize_elgg', 'js/framework/analytics/config');

	elgg_register_simplecache_view('js/framework/analytics/analyze');
	elgg_register_js('analytics', elgg_get_simplecache_url('js', 'framework/analytics/analyze'), 'footer', 1000);

	if (!elgg_is_admin_logged_in()) {

		elgg_load_js('analytics');

		elgg_register_event_handler('init', 'system', __NAMESPACE__ . '\\Segmentio::identify');
		elgg_register_event_handler('login', 'user', __NAMESPACE__ . '\\Segmentio::alias', 999);

		elgg_register_event_handler('all', 'object', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);
		elgg_register_event_handler('all', 'group', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);
		elgg_register_event_handler('all', 'user', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);
		elgg_register_event_handler('all', 'annotation', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);
		//elgg_register_event_handler('all', 'metadata', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);
		elgg_register_event_handler('all', 'relationship', __NAMESPACE__ . '\\Segmentio::trackEvent', 999);

		elgg_register_plugin_hook_handler('view', 'all', __NAMESPACE__ . '\\Segmentio::trackView', 999);

		elgg_register_plugin_hook_handler('action', 'all', __NAMESPACE__ . '\\Segmentio::trackAction', 999);
		elgg_register_plugin_hook_handler('forward', 'all', __NAMESPACE__ . '\\Segmentio::trackForward', 999);
		elgg_register_plugin_hook_handler('route', 'all', __NAMESPACE__ . '\\Segmentio::trackRouter', 999);

		elgg_register_plugin_hook_handler('track', 'system', __NAMESPACE__ . '\\Segmentio::trackCustom', 999);

		elgg_register_event_handler('shutdown', 'system', __NAMESPACE__ . '\\Segmentio::postData', 999);
	}
}
