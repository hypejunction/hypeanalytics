hypeAnalytics
=============

[Fancy buying me a dinner?!](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=P7QA9CFMENBKA)

hypeAnalytics is a tool for collecting batch data about server-side Elgg events.
This plugin is intended primarily for integrating your site with
[Segment.io](http://segment.io/ "Segment.io"), however can also be used for
implementing custom event storage and representation.


## Features

By default, the plugin will track all operations with entities, annotations,
and relationships, as well as capable of recording page routing, forwarding
and actions.


## Developer Notes

### Custom Events

To add a custom event to the batch, use ```analytics_track_custom_event()```,
e.g. to track a purchase, you could add the following to your action file:

```php

$products = $order->getProducts();
analytics_track_custom_event('Completed Order', array(
	'id' => $order->guid,
	'total' => 43.00,
	'tax' => 2.93,
	'products' => (is_array($products)) ? $products : null,
));

```

The function is defined in the global scope, so no need for namespacing.


### Link and Form tracking (client-side)

To track links and forms, add ```data-trackable-event``` attribute to your
```<a>``` or ```<form>``` tags alongside any properties you want to
pass on by prefixing them with ```data-```.

```php
echo elgg_view('output/url', array(
	'text' => 'Action Call',
	'href' => $action_url,
	'data-trackable-event' => 'Clicked on Action Call',
	'data-call-property' => 'I am the property'
));
```

Note that this data is not recorded server-side, and is not available for take out.

### Properties

To modify user, object and group properties (traits) register custom handlers for
```'analytics:properties', 'user'```, ```'analytics:properties', 'object'``` and
```'analytics:properties', 'group'``` hooks.

Note that user properties for the logged in user are exposed client-side, so use
with care.

### Data Take-Out

If you want to take out tracking data and store it elsewhere register a hook handler
for ```'analytics:data', 'system'``` and grab it from the return value.


## Notes

* Analytics will not be gathered for Admin users

* Tracking action handling, page routing and especially forwarding might generate
significant amount of traffic against your Segment.io (and consequently
analytics providers), so use with care and disable in plugin settings, if
necessary

* You may need to add some translations to describe your custom entity types.