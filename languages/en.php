<?php

namespace hypeJunction\Analytics;

$english = array(
	'analytics:settings:segmentio_read_key' => 'Segment.io Read Key',
	'analytics:settings:segmentio_write_key' => 'Segment.io Write Key',
	'analytics:admin_notice:segmentio_keys' => 'Please add your Segment.io keys in ' . PLUGIN_ID . ' plugin settings',
	'analytics:settings:track_action' => 'Track action handling',
	'analytics:settings:track_forward' => 'Track forwarding',
	'analytics:settings:track_route' => 'Track page routing',
	'analytics_event::create::object' => 'Created',
	'analytics_event::update::object' => 'Updated',
	'analytics_event::delete::object' => 'Deleted',
	'analytics_event::create::user' => 'Created',
	'analytics_event::update::user' => 'Updated',
	'analytics_event::delete::user' => 'Deleted',
	'analytics_event::create::group' => 'Created',
	'analytics_event::update::group' => 'Updated',
	'analytics_event::delete::group' => 'Deleted',
	'analytics_event::publish::object' => 'Published',
	'analytics_event::login::user' => 'Logged in',
	'analytics_event::register::user' => 'Registered',
	'analytics_item' => 'an item',
	'analytics_item:object' => 'an object',
	'analytics_item:user' => 'a user',
	'analytics_item:group' => 'a group',
	'analytics_item:metadata' => 'metadata',
	'analytics_item:relationship' => 'a relationship',
	'analytics_item:annotation' => 'an annotation',
	'analytics_item:object:blog' => 'a blog',
	'analytics_item:object:bookmarks' => 'a bookmark',
	'analytics_item:object:page' => 'a page',
	'analytics_item:object:page_top' => 'a page',
	'analytics_item:object:thewire' => 'a wire post',
	'analytics_action' => 'Performed an action %s',
	'analytics_forward' => 'Was forwarded by %s',
	'analytics_router' => 'Was routed by %s',
	'analytics_view:full' => 'Viewed profile of %s',
	'analytics_view:summary' => 'Viewed summary listing of %s',
);

add_translation('en', $english);
