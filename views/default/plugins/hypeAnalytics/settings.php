<?php

namespace hypeJunction\Analytics;

$entity = elgg_extract('entity', $vars);

echo '<div>';
echo '<label>' . elgg_echo('analytics:settings:segmentio_read_key') . '</label>';
echo elgg_view('input/text', array(
	'name' => 'params[segmentio_read_key]',
	'value' => $entity->segmentio_read_key
));
echo '</div>';

echo '<div>';
echo '<label>' . elgg_echo('analytics:settings:segmentio_write_key') . '</label>';
echo elgg_view('input/text', array(
	'name' => 'params[segmentio_write_key]',
	'value' => $entity->segmentio_write_key
));
echo '</div>';

echo '<div>';
echo '<label>' . elgg_echo('analytics:settings:track_action') . '</label>';
echo elgg_view('input/dropdown', array(
	'name' => 'params[track_action]',
	'value' => $entity->track_action,
	'options_values' => array(
		0 => elgg_echo('option:no'),
		1 => elgg_echo('option:yes')
	)
));
echo '</div>';

echo '<div>';
echo '<label>' . elgg_echo('analytics:settings:track_forward') . '</label>';
echo elgg_view('input/dropdown', array(
	'name' => 'params[track_forward]',
	'value' => $entity->track_forward,
	'options_values' => array(
		0 => elgg_echo('option:no'),
		1 => elgg_echo('option:yes')
	)
));
echo '</div>';

echo '<div>';
echo '<label>' . elgg_echo('analytics:settings:track_route') . '</label>';
echo elgg_view('input/dropdown', array(
	'name' => 'params[track_route]',
	'value' => $entity->track_route,
	'options_values' => array(
		0 => elgg_echo('option:no'),
		1 => elgg_echo('option:yes')
	)
));
echo '</div>';